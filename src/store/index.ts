import Vue from 'vue';
import Vuex from 'vuex';
import CurrentUserModule from './current-user.module';
import CurrentProjectModule from './current-project.module';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    currentUserModule: CurrentUserModule,
    currentProjectModule: CurrentProjectModule
  }
});
