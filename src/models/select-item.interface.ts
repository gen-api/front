export default interface SelectItemInterface {
    text: string | number | object;
    value: string | number | object | boolean;
}