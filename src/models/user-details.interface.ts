import ErrorBuilderInterface from './error-builder.interface';

export default interface UserDetailsInterface {
    id: number | null;
    name: string;
    login: string;
    email: string;
    youAre: string;
    companyName?: string;
    password: string;
    role: string;
    errors: ErrorBuilderInterface | null;
}