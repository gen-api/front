export default interface CurrentUser {
    id: number;
    login: string;
    email: string;
    name: string;
    role: string;
}