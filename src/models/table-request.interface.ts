export default interface TableRequestInterface {
    query: {};
    limit: number;
    ascending: number;
    page: number;
    byColumn: number;
    orderBy: string | null;
}

export function buildTableRequest(nativeRequest: any, query: any): TableRequestInterface {
    const request: TableRequestInterface = {
        query: {},
        limit: 0,
        ascending: 0,
        page: 0,
        byColumn: 0,
        orderBy: null
    }

    if(nativeRequest.orderBy !== undefined) request.orderBy = nativeRequest.orderBy;
    request.query = query;

    request.limit = nativeRequest.limit;
    request.ascending = nativeRequest.ascending;
    request.page = nativeRequest.page;

    return request;
}