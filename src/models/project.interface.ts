import ErrorBuilderInterface from './error-builder.interface';

export default class ProjectInterface {
    public id: number | null;
    public name: string;
    public userId: number;
    public auth: boolean;
    public active: boolean;
    public errors: ErrorBuilderInterface | null;
}