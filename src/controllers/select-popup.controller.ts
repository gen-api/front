import SelectItemInterface from '../models/select-item.interface';

export default class SelectPopupController {
    public opened: boolean = false;
    public title: string;
    public label?: string;
    public items: string[] | SelectItemInterface[];
    public defaultValue?: any;
    public multiple?: boolean;

    private resolve: (value: any) => void;
    // private reject: () => void;

    public open(title: string, items: string[] | SelectItemInterface[], label?: string, defaultValue?: any, multiple?: boolean): Promise<any> {
        this.title = title;
        this.items = items;
        this.label = label;
        this.defaultValue = defaultValue;
        this.multiple = multiple;
        this.opened = true;

        return new Promise<any>((resolve, reject)=> {
            this.resolve = resolve;
        })
    }

    public close(): void {
        this.opened = false;
        // this.resolve();
    }

    public select(): void {
        this.resolve(this.defaultValue);
    }
}