const YouAreConstants = {
    STUDENT: 'STUDENT',
    DEVELOPER: 'DEVELOPER',
    BUSINESSMAN: 'BUSINESSMAN'
}

export default YouAreConstants;