import BaseService from './base.service';
import BodyInterface from '../models/body.interface';
import RowInfoInterface from '@/models/row-info.interface';
import TableRequestInterface from '@/models/table-request.interface';
import TableResponseInterface from '@/models/table-response.interface';

export default class BodyService extends BaseService {

    private url = '/back/api/body';

    public getRows(reauest: TableRequestInterface): Promise<TableResponseInterface> {
        return this.post<TableResponseInterface>(`${this.url}/get-rows`, reauest);
    }

    public getInfoRows(projectId: number): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(`${this.url}/get-info-rows?projectId=${projectId}`);
    }

    public getAllInfoRows(): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(`${this.url}/get-all-info-rows`);
    }

    public getById(id: number): Promise<BodyInterface> {
        return this.get<BodyInterface>(this.url + `/${id}`);
    }

    public getByModel(projectId: number, modelId: number): Promise<BodyInterface[]> {
        return this.get<BodyInterface[]>(this.url + `/get-by-model?projectId=${projectId}&modelId=${modelId}`);
    }

    public getPreviewById(id: number): Promise<any> {
        return this.get(this.url + `/preview/${id}`);
    }

    public create(body: BodyInterface): Promise<BodyInterface> {
        return this.post<BodyInterface>(this.url + '/create', body);
    }

    public update(body: BodyInterface): Promise<BodyInterface> {
        return this.put<BodyInterface>(this.url + '/update', body);
    }

    public remove(id: number): Promise<BodyInterface> {
        return this.delete<BodyInterface>(`${this.url}/delete/${id}`);
    }

}