import BaseService from './base.service';
import ProjectClientInterface from '@/models/project-client.interface';
import TableResponseInterface from '@/models/table-response.interface';
import TableRequestInterface from '@/models/table-request.interface';
import RowInfoInterface from '@/models/row-info.interface';

export default class ProjectClientsService extends BaseService {

    private url = '/back/api/project-clients';

    public getInfoRowsByProject(projectId: number): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(`${this.url}/get-info-rows-by-project`, {params: {projectId: projectId}});
    }

    public getInfoRows(): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(`${this.url}/get-info-rows`);
    }

    public getRows(request: TableRequestInterface): Promise<TableResponseInterface> {
        return this.post<TableResponseInterface>(`${this.url}/get-rows`, request);
    }

    public create(projectId: number, password: string): Promise<ProjectClientInterface> {
        return this.post<ProjectClientInterface>(`${this.url}/create?projectId=${projectId}&password=${password}`);
    }

    public remove(clientId: string): Promise<void> {
        return this.delete<void>(`${this.url}/delete/${clientId}`);
    }

}