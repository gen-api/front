import ErrorBuilderInterface from './error-builder.interface';

export default interface UserInterface {
    id: number | null;
    name: string;
    login: string;
    email: string;
    youAre: string;
    companyName?: string;
    role: string;
    errors: ErrorBuilderInterface | null;
}