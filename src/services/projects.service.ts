import BaseService from './base.service';
import ProjectInterface from '../models/project.interface';
import RowInfoInterface from '@/models/row-info.interface';
import TableResponseInterface from '@/models/table-response.interface';
import TableRequestInterface from '@/models/table-request.interface';

export default class ProjectsService extends BaseService {

    private url = '/back/api/projects';

    public getRows(request: TableRequestInterface): Promise<TableResponseInterface> {
        return this.post<TableResponseInterface>(this.url + `/get-rows`, request);
    }

    public getInfoRows(): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(this.url + `/get-info-rows`);
    }

    public getInfoRowsByModel(modelId: number): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(`${this.url}/get-info-rows-by-model?modelId=${modelId}`);
    }

    public getById(id: number): Promise<ProjectInterface> {
        return this.get<ProjectInterface>(this.url + `/${id}`);
    }

    public create(project: ProjectInterface): Promise<ProjectInterface> {
        return this.post<ProjectInterface>(this.url + '/create', project);
    }

    public update(project: ProjectInterface): Promise<ProjectInterface> {
        return this.put<ProjectInterface>(this.url + '/update', project);
    }

    public remove(id: number): Promise<ProjectInterface> {
        return this.delete<ProjectInterface>(`${this.url}/delete/${id}`);
    }

}