import development from './development.environment';
import production from './production.environment';

const env = {
    development: development,
    production: production
}

export default env[process.env.NODE_ENV];