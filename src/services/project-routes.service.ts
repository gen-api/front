import BaseService from './base.service';
import ProjectRouteInterface from '../models/project-route.interface';
import RowInfoInterface from '@/models/row-info.interface';
import TableRequestInterface from '@/models/table-request.interface';
import TableResponseInterface from '@/models/table-response.interface';

export default class ProjectRoutesService extends BaseService {

    private url = '/back/api/routes';

    public getRows(request: TableRequestInterface): Promise<TableResponseInterface> {
        return this.post<TableResponseInterface>(`${this.url}/get-rows`, request);
    }

    public getInfoRows(projectId: number): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(`${this.url}/get-info-rows?projectId=${projectId}`);
    }

    public getById(id: number): Promise<ProjectRouteInterface> {
        return this.get<ProjectRouteInterface>(this.url + `/${id}`);
    }

    public getResponseTypes(): Promise<string[]> {
        return this.get<string[]>(this.url + '/get-response-types');
    }

    public create(route: ProjectRouteInterface): Promise<ProjectRouteInterface> {
        return this.post<ProjectRouteInterface>(this.url + '/create', route);
    }

    public update(route: ProjectRouteInterface): Promise<ProjectRouteInterface> {
        return this.put<ProjectRouteInterface>(this.url + '/update', route);
    }

    public remove(id: number): Promise<ProjectRouteInterface> {
        return this.delete(`${this.url}/delete/${id}`);
    }

}