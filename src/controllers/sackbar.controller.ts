export default class SnackbarController {

    public text: string = '';
    public color: string = 'black';
    public opened: boolean = false;

    public open(text: string): void {
        this.color = 'black';
        this.callOpen(text);
    }

    public openError(text: string): void {
        this.color = 'red';
        this.callOpen(text);
    }

    public openSuccess(text: string): void {
        this.color = 'success';
        this.callOpen(text);
    }

    private callOpen(text: string): void {
        this.text = text;
        this.opened = true;
    }

}