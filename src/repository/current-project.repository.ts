export default class CurrentProjectRepository {

    public static setProjectId(userId: string | number, projectId: string | number): void {
        const resultPID: string = (typeof projectId === 'string') ? projectId : String(projectId);
        localStorage.setItem(userId + ':currentProjectId', resultPID);
    }

    public static getProjectId(userId: string | number): any {
        return localStorage.getItem(userId + ':currentProjectId');
    }

    public static removeProjectId(userId: string | number): void {
        localStorage.removeItem(userId + ':currentProjectId');
    }

}