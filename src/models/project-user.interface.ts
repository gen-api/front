import ErrorBuilderInterface from './error-builder.interface';

export default interface ProjectUserInterface {
    id: number | null;
    login: string;
    clientId: string;
    projectId: number;
    projectName: string;
    errors: ErrorBuilderInterface | null;
}