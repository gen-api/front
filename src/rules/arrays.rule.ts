import * as RULES from './base.rule';

export const USER_LOGIN: any[] = [RULES.REQUIRED, RULES.LOGIN];
export const USER_EMAIL: any[] = [RULES.REQUIRED, RULES.EMAIL];
export const USER_PASSWORD: any[] = [RULES.REQUIRED, RULES.PASSWORD];
export const MODEL_FIELD_NAME: any[] = [RULES.REQUIRED, RULES.FIELD_NAME];

export const valid = (rules: any[], value: any): boolean | string => {
    for(let i=0; i<rules.length; i++) {
        const result: boolean | string = rules[i](value);
        if(typeof result === 'string') return result;
    }
    return true;
};