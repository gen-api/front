import Vue from 'vue';
import Router from 'vue-router';
import navigation from './navigation';
import { mapToRouteConfig } from './models/nav-item.interface';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: mapToRouteConfig(navigation)
});
