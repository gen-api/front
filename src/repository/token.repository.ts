export default class TokenRepository {

    public static setToken(token: any): void {
        const result: string = (typeof token === 'string') ? token : JSON.stringify(token);
        localStorage.setItem('token', result);
    }

    public static getToken(): any {
        return localStorage.getItem('token');
    }

    public static removeToken(): void {
        localStorage.removeItem('token');
    }

}