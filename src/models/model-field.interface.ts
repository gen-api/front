export default interface ModelFieldInterface {
    name: string;
    type: string;
    required: boolean;
}