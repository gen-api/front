import ErrorBuilderInterface from './error-builder.interface';

export default interface ProjectRouteInterface {
    id: number | null;
    name: string;
    route: string;
    projectId: number;
    bodyId: number | null;
    listId: number | null;
    responseType: string;
    errors: ErrorBuilderInterface | null;
}