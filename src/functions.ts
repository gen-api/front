export function enumToArray(enm: any): string[] {
    const fields: string[] = [];
    Object.keys(enm).forEach(item => {
        if(item.replace(/\d+/, '').length === 0) return;
        fields.push(item);
    });
    return fields;
}

export function getModelIdByType(type: string): number {
    return +type.replace(/MODEL\(|\)/g, '');
}

export function getModelTypeById(id: number): string {
    return 'MODEL(' + id + ')';
}

export function getModelListIdByType(type: string): number {
    return +type.replace(/MODEL\(|\)\[\]/g, '');
}

export function getModelListTypeById(id: number): string {
    return 'MODEL(' + id + ')[]';
}