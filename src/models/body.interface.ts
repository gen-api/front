import ModelFieldInterface from './model-field.interface';
import ErrorBuilderInterface from './error-builder.interface';

export default interface BodyInterface {
    id: number | null;
    name: string;
    modelId: number | null;
    projectId: number;
    value: any | any[];
    modelName: string;
    model: ModelFieldInterface[];
    projectName: string;
    errors: ErrorBuilderInterface | null;
}

export function getIdByValue(value: string): number {
    return +value.replace(/BODY\(|\)/, '');
}

export function getValueById(id: number | string): string {
    return 'BODY(' + id + ')';
}