export const REQUIRED = (value: any) => String(value).length > 0 && value !== null || 'Required.';

export const EMAIL = (value: string) => {
    const pattern = /^\S+@\S+\.\w{2,}$/;
    return pattern.test(value) || value.length === 0 || value === 'undefined' || 'Invalid';
};

export const FIELD_NAME = (value: string) => /^\w{1,50}$/.test(value) && value !== '_id' && value !== 'id' || value === 'undefined' || 'Invalid';

export const LOGIN = (value: string) => /^\w{4,50}$/.test(value) || value === 'undefined' || 'Invalid';

export const PASSWORD = (value: string) => /^\w{5,50}$/.test(value) && /\d+/.test(value) || value === 'undefined' || 'Invalid';

export const INTAGER = (value: string | number) => {
    const input = String(value);
    return /^(-)?\d+$/.test(input) && +input <= 2147483647 && +input >= -2147483648 || input.length === 0 || input === 'undefined' || 'Invalid';
};

export const FLOAT = (value: string | number) => {
    const input = String(value);
    return /^(-)?\d+(\.\d+)?$/.test(input) && +input <= 2147483647 && +input >= -2147483648 || input.length === 0 || input === 'undefined' || 'Invalid';
};

export const BOOLEAN = (value: string | boolean) => {
    const input = String(value);
    return /^true$|^false$/.test(input) || input.length === 0 || input === 'undefined' || 'Invalid';
};

export const MODEL = (value: string) => {
    return /^MODEL\(\d+\)$/.test(value) || value.length === 0 || value === 'undefined' || 'Invalid';
};

export const MODEL_LIST = (value: string) => {
    return /^MODEL\(\d+\)\[\]$/.test(value) || value.length === 0 || value === 'undefined' || 'Invalid';
}

export const BASE64 = (value: string) => {
    return /^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$/.test(value) || value.length === 0 || value === 'undefined' || 'Invalid';
};

export const IMAGE = (value: string) => {
    return /^data:image\/[a-z]+;base64,([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$/.test(value) || value.length === 0 || value === 'undefined' || 'Invalid';
};

export const ROUTE = (value: string) => {
    return value.replace(/\/|\w+|-/g, '').length === 0 && value.search('//') === -1 && value[value.length-1] !== '/' && value[0] === '/' || value === undefined || 'Invalid';
};