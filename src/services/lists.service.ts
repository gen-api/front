import BaseService from './base.service';
import ListInterface from '../models/list.interface';
import RowInfoInterface from '@/models/row-info.interface';
import TableRequestInterface from '@/models/table-request.interface';
import TableResponseInterface from '@/models/table-response.interface';

export default class ListService extends BaseService {

    private url = '/back/api/lists';

    public getRows(request: TableRequestInterface): Promise<TableResponseInterface> {
        return this.post<TableResponseInterface>(`${this.url}/get-rows`, request);
    }

    public getInfoRows(projectId: number): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(this.url + `/get-info-rows?projectId=${projectId}`);
    }

    public getById(id: number): Promise<ListInterface> {
        return this.get<ListInterface>(this.url + `/${id}`);
    }

    public getByModelId(modelId: number, projectId: number): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(`${this.url}/by-model?modelId=${modelId}&projectId=${projectId}`);
    }

    public getByType(type: string, projectId: number): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(`${this.url}/by-type?type=${type}&projectId=${projectId}`);
    }

    public getPreviewById(id: number): Promise<any> {
        return this.get(this.url + `/preview/${id}`);
    }

    public create(list: ListInterface): Promise<ListInterface> {
        return this.post<ListInterface>(this.url + '/create', list);
    }

    public update(list: ListInterface): Promise<ListInterface> {
        return this.put<ListInterface>(this.url + '/update', list);
    }

    public remove(id: number): Promise<ListInterface> {
        return this.delete<ListInterface>(`${this.url}/delete/${id}`);
    }

}