import UserInterface from '../models/user.interface';


export const ACTION_LOGIN = 'ACTION_LOGIN';
export const ACTION_LOGOUT = 'ACTION_LOGOUT';


export const SET_CURRENT_USER = 'SET_CURRENT_USER';


export const GET_CURRENT_USER = 'GET_CURRENT_USER';


const module = {
    state: {
        currentUser: null
    },
    getters: {
        [GET_CURRENT_USER]: (state: any) => {
            return state.currentUser !== null ? {...state.currentUser} : null;
        }
    },
    mutations: {
        [SET_CURRENT_USER]: (state: any, user: UserInterface) => {
            return state.currentUser = user;
        }
    },
    actions: {

        [ACTION_LOGIN]: (action: any, user: UserInterface) => {
            action.commit(SET_CURRENT_USER, user);
        },

        [ACTION_LOGOUT]: (action: any) => {
            action.commit(SET_CURRENT_USER, null);
        }

    }

}

export default module;